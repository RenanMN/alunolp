unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls, Unit2;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Bt_Atualizar: TButton;
    Navegador: TNetHTTPClient;
    Bt_Inserir: TButton;
    procedure Bt_AtualizarClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Bt_InserirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TPersonagem = class(TObject)
  Nome:String;
  Profissao: String;
  Nivel: Integer;
  Vida: Integer;
  Defesa: Integer;
  Ataque: Integer;
  end;

var
  Form1: TForm1;
  Form2: TForm2;
  Personagem : TPersonagem;

implementation

{$R *.dfm}

procedure TForm1.Bt_InserirClick(Sender: TObject);
begin
Form2 := TForm2.Create(Application);
Form2.Visible := True;
end;

procedure TForm1.Bt_AtualizarClick(Sender: TObject);
var conteudo, personagemString:String;
    listaPersonagens:TArray<String>;
    listaAtributos:TArray<String>;
begin
 ListBox1.Clear;

 conteudo := Navegador.Get('https://venson.net.br/ws/personagens').ContentAsString;
 listaPersonagens := conteudo.Split(['&']);
   for personagemString in listaPersonagens do
     Begin
       Personagem := TPersonagem.Create();
       listaAtributos := personagemString.Split([';']);

       Personagem.Nome := listaAtributos[0];
       Personagem.Profissao := listaAtributos[1];
       Personagem.Nivel := listaAtributos[2].ToInteger;
       Personagem.Vida := listaAtributos[3].ToInteger;
       Personagem.Defesa := listaAtributos[4].ToInteger;
       Personagem.Ataque := listaAtributos[5].ToInteger;

       ListBox1.Items.AddObject(Personagem.Nome, Personagem);
     End;
end;

procedure TForm1.ListBox1DblClick(Sender: TObject);
 begin
  Personagem := TPersonagem(ListBox1.Items.Objects[ListBox1.ItemIndex]);
  ShowMessage(Personagem.Nome+ ' � um(a) ' +Personagem.Profissao+ ' de n�vel ' +IntToStr(Personagem.Nivel));
 end;

end.