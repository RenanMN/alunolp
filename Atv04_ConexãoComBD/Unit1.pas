unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, Data.DB, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.Mask;

type
  TForm1 = class(TForm)
    AbrirTela: TButton;
    Inserir: TButton;
    deletar: TButton;
    DBLookupListBox1: TDBLookupListBox;
    LB_nome: TLabel;
    Label1: TLabel;
    DBText2: TDBText;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    DBText1: TDBText;
    Panel2: TPanel;
    Button1: TButton;
    procedure AbrirTelaClick(Sender: TObject);
    procedure InserirClick(Sender: TObject);
    procedure deletarClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.AbrirTelaClick(Sender: TObject);
begin
      DataModule2.FDQuery1.Open;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    DataModule2.FDQuery1.post;
end;

procedure TForm1.deletarClick(Sender: TObject);
begin
      DataModule2.FDQuery1.Delete;
end;

procedure TForm1.InserirClick(Sender: TObject);
begin
       DataModule2.FDQuery1.Append;
end;

end.
