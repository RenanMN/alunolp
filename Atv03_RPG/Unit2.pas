unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent;

type
  TForm2 = class(TForm)
    EdNome: TEdit;
    EdClasse: TEdit;
    EdNivel: TEdit;
    EdAtaque: TEdit;
    EdDefesa: TEdit;
    EdVida: TEdit;
    BtnInsert: TButton;
    BtnVerificar: TButton;
    LbNome: TLabel;
    LbClasse: TLabel;
    LbNivel: TLabel;
    LbVida: TLabel;
    LbAtaque: TLabel;
    LbDefesa: TLabel;
    Navegador: TNetHTTPClient;
    procedure BtnVerificarClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.BtnInsertClick(Sender: TObject);
  var nivel, Def, Atq, Vida: Integer;
      nome, Classe, conteudo: String;

  begin
    nivel:= StrtoInt(EdNivel.Text);
    Def:= StrtoInt(EdDefesa.Text);
    Vida:= StrtoInt(EdVida.Text);
    Atq:= StrtoInt(EdAtaque.Text);
    nome:= EdNome.Text;
    Classe:= EdClasse.Text;
    conteudo := Navegador.Get('https://venson.net.br/ws/personagens/?op=salvar&nome='
    +nome+'&classe='+Classe+'&nivel='+(IntToStr(nivel))+'&ata='+(IntToStr(Atq))+
    '&def='+(IntToStr(Def))+'&life='+(IntToStr(Vida))).ContentAsString;
  end;

procedure TForm2.BtnVerificarClick(Sender: TObject);
var
mult, nivel, Atq, Def, Vida: integer;
begin
nivel:= StrtoInt(EdNivel.Text);
Def:= StrtoInt(EdDefesa.Text);
Vida:= StrtoInt(EdVida.Text);
Atq:= StrtoInt(EdAtaque.Text);
mult:= nivel * 10;


  if nivel > 10 then
   Begin
    ShowMessage('Vida maior que esperado');
    BtnInsert.Enabled:= False;
   end
   else BtnInsert.Enabled:=True;
   ShowMessage('Nivel OK');

  if atq > mult then
   Begin
    ShowMessage('Ataque maior que esperado');
    BtnInsert.Enabled:= False;
   end
   else BtnInsert.Enabled:=True;
   ShowMessage('Ataque OK');

  if Def < 1 then
    Begin
     ShowMessage('Defesa menor que esperado');
     BtnInsert.Enabled:= False;
    end
    else BtnInsert.Enabled:=True;
    ShowMessage('Defesa OK');

  if Vida > mult then
    Begin
     ShowMessage('Vida maior que esperado');
     BtnInsert.Enabled:= False;
    end
    else BtnInsert.Enabled:=True;
    ShowMessage('Vida OK');
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
BtnInsert.Enabled:= False;
end;

end.
