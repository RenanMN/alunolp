object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 141
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'DejaVu Sans Condensed'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 19
  object Button1: TButton
    Left = 280
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 160
    object Forms: TMenuItem
      Caption = 'Formul'#225'rios'
      OnClick = FormsClick
      object Form1: TMenuItem
        Caption = 'Formul'#225'rio 1'
      end
      object Formulrio21: TMenuItem
        Caption = 'Formul'#225'rio 2'
        OnClick = Formulrio21Click
      end
      object Formulrio31: TMenuItem
        Caption = 'Formul'#225'rio 3'
        OnClick = Formulrio31Click
      end
    end
  end
end
