object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 213
  Width = 402
  object IWDataModulePool1: TIWDataModulePool
    PoolCount = 20
    Active = False
    Version = '2.0.0'
    Left = 16
    Top = 8
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 176
    Top = 8
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokemon'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 96
    Top = 8
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 240
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 296
    Top = 8
  end
  object FDQuery2: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinador')
    Left = 240
    Top = 72
  end
  object DataSource2: TDataSource
    DataSet = FDQuery2
    Left = 296
    Top = 72
  end
end
