unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus, Unit2, Unit3;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Button1: TButton;
    Forms: TMenuItem;
    Form1: TMenuItem;
    Formulrio21: TMenuItem;
    Formulrio31: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormsClick(Sender: TObject);
    procedure Formulrio21Click(Sender: TObject);
    procedure Formulrio31Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Form2.Visible := true;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   Form2 := TForm2.Create(Application);
end;

procedure TForm1.FormsClick(Sender: TObject);
begin
   Form1.Visible := True;
   Form2.Hide;
end;

procedure TForm1.Formulrio21Click(Sender: TObject);
begin
   Form2.Visible := true;
   Form1.Visible := false;

end;

procedure TForm1.Formulrio31Click(Sender: TObject);
begin
  Form3.Visible := true;
  Form1.Visible := false;
end;

end.
