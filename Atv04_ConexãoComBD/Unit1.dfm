object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 328
  ClientWidth = 532
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 193
    Top = 8
    Width = 208
    Height = 121
    Align = alCustom
    TabOrder = 4
    object Label2: TLabel
      Left = 14
      Top = 17
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label3: TLabel
      Left = 14
      Top = 44
      Width = 23
      Height = 13
      Caption = 'Nivel'
    end
    object Label4: TLabel
      Left = 12
      Top = 68
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object DBEdit1: TDBEdit
      Left = 64
      Top = 14
      Width = 121
      Height = 21
      Hint = 'Nome'
      DataField = 'nome'
      DataSource = DataModule2.DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 64
      Top = 41
      Width = 121
      Height = 21
      DataField = 'nivel'
      DataSource = DataModule2.DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 64
      Top = 68
      Width = 121
      Height = 21
      DataField = 'id_treinador'
      DataSource = DataModule2.DataSource1
      TabOrder = 2
    end
    object Button1: TButton
      Left = 112
      Top = 95
      Width = 75
      Height = 25
      Caption = 'Adicionar'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object AbrirTela: TButton
    Left = 449
    Top = 8
    Width = 75
    Height = 25
    Caption = 'AbrirTela'
    TabOrder = 0
    OnClick = AbrirTelaClick
  end
  object Inserir: TButton
    Left = 449
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = InserirClick
  end
  object deletar: TButton
    Left = 449
    Top = 70
    Width = 75
    Height = 25
    Caption = 'deletar'
    TabOrder = 2
    OnClick = deletarClick
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 179
    Height = 121
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule2.DataSource1
    TabOrder = 3
  end
  object Panel2: TPanel
    Left = 8
    Top = 144
    Width = 179
    Height = 89
    TabOrder = 5
    object LB_nome: TLabel
      Left = 0
      Top = 12
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label1: TLabel
      Left = 0
      Top = 31
      Width = 23
      Height = 13
      Caption = 'Nivel'
    end
    object DBText1: TDBText
      Left = 57
      Top = 9
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule2.DataSource1
    end
    object DBText2: TDBText
      Left = 57
      Top = 32
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule2.DataSource1
    end
  end
end
