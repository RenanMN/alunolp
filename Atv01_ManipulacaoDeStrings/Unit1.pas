unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    MemoPrincipal: TMemo;
    MemoConsole: TMemo;
    ButtonLength: TButton;
    ButtonContains: TButton;
    ButtonTrim: TButton;
    ButtonLowerCase: TButton;
    ButtonUpperCase: TButton;
    ButtonReplace: TButton;
    procedure ButtonLengthClick(Sender: TObject);
    procedure ButtonContainsClick(Sender: TObject);
    procedure ButtonTrimClick(Sender: TObject);
    procedure ButtonLowerCaseClick(Sender: TObject);
    procedure ButtonUpperCaseClick(Sender: TObject);
    procedure ButtonReplaceClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  conteudo : string;

implementation

{$R *.dfm}

procedure TForm1.ButtonContainsClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  if conteudo.Contains('s') then
  begin
  	MemoConsole.Text := 'Encontrou!';  
  end
  else
  begin
  	MemoConsole.Text := 'N�o Encontrou!';
  end;
end;

procedure TForm1.ButtonLengthClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  MemoConsole.Text := conteudo.Length.ToString;	
end;

procedure TForm1.ButtonLowerCaseClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  MemoConsole.Text := conteudo.LowerCase(conteudo);
end;

procedure TForm1.ButtonReplaceClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  MemoConsole.Text := conteudo.Replace('s', 't');
end;

procedure TForm1.ButtonTrimClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  MemoConsole.Text := conteudo.Trim();
end;

procedure TForm1.ButtonUpperCaseClick(Sender: TObject);
begin
	conteudo := MemoPrincipal.Text;
  MemoConsole.Text := conteudo.UpperCase(conteudo);
end;

end.
