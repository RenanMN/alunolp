object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 470
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = '@Arial Unicode MS'
  Font.Style = [fsItalic]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 21
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 353
    Height = 329
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = '@Arial Unicode MS'
    Font.Style = [fsItalic]
    ItemHeight = 21
    ParentFont = False
    TabOrder = 0
    OnDblClick = ListBox1DblClick
  end
  object Bt_Atualizar: TButton
    Left = 248
    Top = 351
    Width = 113
    Height = 25
    Caption = 'Atualizar Lista'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = '@Arial Unicode MS'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    OnClick = Bt_AtualizarClick
  end
  object Bt_Inserir: TButton
    Left = 8
    Top = 351
    Width = 113
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = '@Arial Unicode MS'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 2
    OnClick = Bt_InserirClick
  end
  object Navegador: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 488
    Top = 424
  end
end
